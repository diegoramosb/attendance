package com.diegoramosb.attendance

import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.*

class MissingStudentsActivity : AppCompatActivity() {

    private lateinit var date: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_missing_students)
        val sdf = SimpleDateFormat("dd-M-yyyy")
        date = sdf.format(Date())
    }


    private fun getAllStudents() {
        val url =
            "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentList"
        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = JsonObjectRequest(url, null,
            Response.Listener { response ->
                getAttendanceList(response)
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, error.message, Toast.LENGTH_LONG)
            }
        )
        queue.add(jsonObjectRequest)
    }

    private fun getAttendanceList(students: JSONObject) {
        val url =
            "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/${date}/students"
        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = JsonObjectRequest(url, null,
            Response.Listener { response ->
                getMissingStudents(response, students)
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, error.message, Toast.LENGTH_LONG)
            }
        )
        queue.add(jsonObjectRequest)
    }

    private fun getMissingStudents(allStudents: JSONObject, studentsToday: JSONObject) {
        var studentList = allStudents.getJSONArray("documents")
        var studentListToday = studentsToday.getJSONArray("documents")
        var missingStudents = JSONArray()

        for (i in 0..studentList.length() - 1) {
            var student = studentList.getJSONObject(i).getJSONObject("fields")
            var studentCode = student.getJSONObject("code").getString("stringValue")
            var missing = true
            for (j in 0..studentListToday.length() - 1) {
                var student2 = studentListToday.getJSONObject(j).getJSONObject("fields")
                var studentCode2 = student2.getJSONObject("code").getString("stringValue")
                if (studentCode == studentCode2 ) {
                    missing = false
                }
            }
            if (missing) {
                missingStudents.put(student)
            }
        }
        saveMissingStudents(missingStudents)
    }

    private fun saveMissingStudents(students: JSONArray) {
        val file = File(filesDir.path + "missing_students_$date.csv")

        try {
            file.appendText("name, student_code \n")
            for(i in 0..students.length() - 1) {
                var student = students.getJSONObject(i)
                var studentName = student.getJSONObject("name").getString("stringValue")
                var studentCode = student.getJSONObject("code").getString("stringValue")
                file.appendText("${studentName}, ${studentCode} \n")
            }
        } catch(e: IOException) {
            Toast.makeText(this, "Error saving file", Toast.LENGTH_LONG)
        }

    }
}
