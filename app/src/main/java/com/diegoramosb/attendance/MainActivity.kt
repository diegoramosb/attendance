package com.diegoramosb.attendance

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var textView: TextView

    private lateinit var btnDiscovery: Button

    private lateinit var btnMissing: Button

    private lateinit var progressBar: ProgressBar

    private lateinit var ivCheck: ImageView

    private val macAddress = "38:80:DF:C1:43:5A"

    private val code = "201626242"

    private val bluetooth = BluetoothAdapter.getDefaultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById(R.id.tvVerify)
        btnDiscovery = findViewById(R.id.btnDiscovery)
        btnMissing = findViewById(R.id.btnMissing)
        progressBar = findViewById(R.id.progressBar)
        ivCheck = findViewById(R.id.ivCheck)
        ivCheck.visibility = View.INVISIBLE

        btnDiscovery.setOnClickListener {
            toggleDiscovery()
        }

        btnMissing.setOnClickListener {
            showMissingStudentsAct()
        }
    }

    private fun showMissingStudentsAct() {
        val intent = Intent(this, MissingStudentsActivity::class.java)
        intent.putExtra("asdf", "asdf")
        startActivity(intent)
    }

    private fun toggleDiscovery() {
        if (!bluetooth.isEnabled) {
            bluetooth.enable()
            bluetooth.startDiscovery()
            btnDiscovery.setText(R.string.discovery_off)
            textView.text = "Entering discovery mode..."
            progressBar.visibility = View.VISIBLE
            Handler().postDelayed({
                getAttendance()
            }, 10000)
        } else {
            bluetooth.cancelDiscovery()
            bluetooth.disable()
            btnDiscovery.setText(R.string.discovery_on)
        }
    }

    private fun getAttendance() {
        textView.text = "Checking database..."
        val sdf = SimpleDateFormat("dd-M-yyyy")
        val date = sdf.format(Date())
        val url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/${date}/students/${code}"
        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = JsonObjectRequest(url, null,
            Response.Listener { response ->
                checkAttendance(response)
            },
            Response.ErrorListener { error ->
                textView.text = "Attendance check failed!"
                progressBar.visibility = View.INVISIBLE
                Toast.makeText(this, error.message, Toast.LENGTH_LONG)
            }
        )
        queue.add(jsonObjectRequest)
    }

    private fun checkAttendance(attendance: JSONObject) {
        try {
            var dbMAC = attendance.getJSONObject("fields").getJSONObject("mac").getString("stringValue")
            var dbCode = attendance.getJSONObject("fields").getJSONObject("code").getString("stringValue")
            if (dbMAC == macAddress && dbCode == code) {
                progressBar.visibility = View.INVISIBLE
                ivCheck.visibility = View.VISIBLE
                textView.text = "Attendance verified"
                textView.setTextColor(resources.getColor(R.color.colorSuccess))
            }
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG)
        }
    }
}